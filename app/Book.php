<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Book
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $title_kana_1
 * @property string|null $title_kana_2
 * @property string|null $sub_title
 * @property string|null $sub_title_kana
 * @property string|null $original_title
 * @property string|null $first_publicate
 * @property string|null $classification
 * @property string|null $letter_type
 * @property bool|null $copyright_flg
 * @property string|null $published_date
 * @property string|null $updated_date
 * @property string|null $bibliography_url
 * @property int|null $person_id
 * @property string|null $role
 * @property string|null $original_text
 * @property string|null $original_text_publisher
 * @property string|null $original_text_publicated
 * @property string|null $input_edition
 * @property string|null $proof_edition
 * @property string|null $inputter
 * @property string|null $proofreader
 * @property string|null $text_url
 * @property string|null $text_updated_at
 * @property string|null $text_character_code
 * @property string|null $text_standard
 * @property string|null $text_update_frequency
 * @property string|null $html_url
 * @property string|null $html_updated_at
 * @property string|null $html_character_code
 * @property string|null $html_standard
 * @property string|null $html_update_frequency
 * @property int|null $word_count
 * @property string|null $sentence
 * @property int|null $traffic
 * @property string|null $category
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBibliographyUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereClassification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCopyrightFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereFirstPublicate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereHtmlCharacterCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereHtmlStandard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereHtmlUpdateFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereHtmlUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereHtmlUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereInputEdition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereInputter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereLetterType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereOriginalText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereOriginalTextPublicated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereOriginalTextPublisher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereOriginalTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereProofEdition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereProofreader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book wherePublishedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereSentence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereSubTitleKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTextCharacterCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTextStandard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTextUpdateFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTextUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTextUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTitleKana1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTitleKana2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereTraffic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereWordCount($value)
 * @mixin \Eloquent
 */
class Book extends Model
{
    protected static function booted()
    {
        static::addGlobalScope('text_url', function (Builder $builder) {
            $builder->where('text_url', '<>', null);
        });

        static::addGlobalScope('copyright_flg', function (Builder $builder) {
            $builder->where('copyright_flg', false);
        });
    }

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
