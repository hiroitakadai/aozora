<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class Content
{

    protected $book = null;

    protected $text = '';

    public $head = '';

    public $chuki = '';

    public $body = '';

    public $tail = '';

    protected $section = 'head';

    protected $chuki_start = false;

    protected $chuki_exit = false;

    protected $is_paragraph = false;

    protected $indent = 0;

    function __construct(Book $book)
    {
        $this->book = $book;

        $url_array = parse_url($this->book->text_url);
        $path_array = pathinfo($url_array['path']);
        $storage_path = $path_array['dirname'] . '/' . substr($path_array['basename'], 0, -4) . '/' . substr($path_array['basename'], 0, -3) . 'txt';
        $this->text = mb_convert_encoding(Storage::get($storage_path), 'UTF-8', 'SJIS');

        $this->parse();
    }

    public function parse()
    {
        $lines = preg_split('/\n/', $this->text);

        foreach ($lines as $line) {
            if ($this->section === 'head') {
                $this->parseHead($line);
            } else if ($this->section === 'chuki') {
                $this->parseChuki($line);
            } else if ($this->section === 'body') {
                $this->parseBody($line);
            } else if ($this->section === 'tail') {
                $this->parseTail($line);
            }
        }
    }

    protected function parseHead(string $text)
    {
        if ($text !== '') {
            // 空行になるまではヘッダー
            $this->head .= $text . "\r\n";
            return;
        }

        if ($text === '') {
            // 空行が出現したら注記
            $this->section = 'chuki';
            return;
        }
    }

    protected function parseChuki(string $text)
    {
        if ($this->chuki_start === false) {
            if (strpos($text, '----------') !== false) {
                // 注記の開始
                $this->chuki_start = true;
                return;
            }

            if ($text === "\r\n") {
                return;
            }

            // 注記が含まれないファイルの場合
            $this->section = 'body';
            $this->parseBody($text);
        }

        if ($this->chuki_start === true && $this->chuki_exit === false) {
            if (strpos($text, '----------') === false) {
                $this->chuki .= $text . "\r\n";
                return;
            }

            if (strpos($text, '----------') !== false) {
                // 注記の終了
                $this->chuki_exit = true;
                return;
            }
        }

        if ($this->chuki_start === true && $this->chuki_exit === true) {
            // 次の行からボディ
            $this->section = 'body';
            return;
        }
    }

    protected function parseBody(string $text)
    {
        if (preg_match('/^底本：/', $text)) {
            // 底本から後書き
            $this->body = rtrim($this->body, '<p>');
            $this->tail .= $text . "\r\n";
            $this->section = 'tail';
            return;
        }

        if ($text === '') {
            if ($this->is_paragraph === true) {
                $this->is_paragraph = false;
                $this->body .= "</p>\r\n";
            } else {
                $this->body .= "<br>\r\n";
            }
            return;
        }

        $text = $this->markupBody($text);

        if ($this->is_paragraph === false) {
            $this->is_paragraph = true;
            $this->body .= '<p>' . $text . "<br>\r\n";
            return;
        }

        if ($this->is_paragraph === true) {
            $this->body .= $text . "<br>\r\n";
        }
    }

    protected function parseTail(string $text)
    {
        $this->tail .= $text . "<br>\r\n";
    }

    protected function markupBody(string $text)
    {
        // レイアウト　１
        // 改丁・改ページ
        $text = preg_replace('/(［＃改丁］|［＃改ページ］)/', '<div>改ページ</div>', $text);
        // 改見開き
        $text = preg_replace('/［＃改見開き］/', '<div>改見開き</div>', $text);
        // 改段
        $text = preg_replace('/［＃改段］/', '<div>改段</div>', $text);

        // レイアウト　２
        // １行だけの字下げ
        // TODO css class pl-?? の追加
        if (preg_match('/［＃(.+)字下げ］/', $text, $str)) {
            $value = mb_convert_kana($str[1], "n", "utf-8");
            $text = preg_replace('/［＃(.+)字下げ］/', '<div class="pl-' . $value . '">', $text) . '</div>';
        }
        // ブロックでの字下げ
        if (preg_match('/［＃ここから(.+)字下げ］/', $text, $str)) {
            $value = mb_convert_kana($str[1], "n", "utf-8");
            $text = preg_replace('/［＃ここから(.+)字下げ］/', '<div class="pl-' . $value . '" >', $text);
            $this->indent += 1;
        }
        if (preg_match('/［＃ここで字下げ終わり］/', $text, $str)) {
            $text = preg_replace('/［＃ここで字下げ終わり］/', str_repeat('</div>', $this->indent), $text);
            $this->indent = 0;
        }
        // TODO 凹凸の複雑な字下げ
        // 地付き
        if (preg_match('/［＃地付き］/', $text)) {
            $text = preg_replace('/［＃地付き］/', '<div class="text_right">', $text) . '</div>';
        }
        $text = preg_replace('/［＃ここから地付き］/', '<div class="text_right">', $text);
        $text = preg_replace('/［＃ここで地付き終わり］/', '</div>', $text);
        // 地寄せ
        if (preg_match('/［＃地から(.+)字上げ］/', $text, $str)) {
            $value = mb_convert_kana($str[1], "n", "utf-8");
            $text = preg_replace('/［＃地から(.+)字上げ］/', '<div class="text_right pr-' . $value . '" />', $text) . '</div>';
        }
        if (preg_match('/［＃ここから地から(.+)字上げ］/', $text, $str)) {
            $value = mb_convert_kana($str[1], "n", "utf-8");
            $text = preg_replace('/［＃ここから地から(.+)字上げ］/', '<div class="text_right pl-' . $value . '" >', $text);
            $this->indent += 1;
        }
        preg_replace('/［＃ここで字上げ終わり］/', '</div>', $text);

        // レイアウト３
        // 左右中央
        $text = preg_replace('/［＃ページの左右中央］/', '<div>ページの左右中央</div>', $text);

        // ●見出し
        // 通常の見出し
        if (preg_match('/［＃「(.+)」は(大|中|小)見出し］/', $text, $str)) {
            if ($str[2] === '大') {
                $text = preg_replace('/' . $str[1] . '/', '<h2>' . $str[1] . '</h2>', $text);
            } else if ($str[2] === '中') {
                $text = preg_replace('/' . $str[1] . '/', '<h3>' . $str[1] . '</h3>', $text);
            } else if ($str[2] === '小') {
                $text = preg_replace('/' . $str[1] . '/', '<h4>' . $str[1] . '</h4>', $text);
            }
            $text = preg_replace('/［＃「(.+)」は(大|中|小)見出し］/', '', $text);
        }
        $text = preg_replace('/(［＃大見出し］|［＃ここから大見出し］)/', '<h2>', $text);
        $text = preg_replace('/(［＃大見出し終わり］|［＃ここで大見出し終わり］)/', '</h2>', $text);
        $text = preg_replace('/(［＃中見出し］|［＃ここから中見出し］)/', '<h3>', $text);
        $text = preg_replace('/(［＃中見出し終わり］|［＃ここで中見出し終わり］)/', '</h3>', $text);
        $text = preg_replace('/(［＃小見出し］|［＃ここから小見出し］)/', '<h4>', $text);
        $text = preg_replace('/(［＃小見出し終わり］|［＃ここで小見出し終わり］)/', '</h4>', $text);
        // 同行見出し
        if (preg_match('/［＃「(.+)」は同行(大|中|小)見出し］/', $text, $str)) {
            if ($str[2] === '大') {
                $text = preg_replace('/' . $str[1] . '/', '<h2>' . $str[1] . '</h2>', $text);
            } else if ($str[2] === '中') {
                $text = preg_replace('/' . $str[1] . '/', '<h3>' . $str[1] . '</h3>', $text);
            } else if ($str[2] === '小') {
                $text = preg_replace('/' . $str[1] . '/', '<h4>' . $str[1] . '</h4>', $text);
            }
            $text = preg_replace('/［＃「(.+)」は同行(大|中|小)見出し］/', '', $text);
        }
        $text = preg_replace('/［＃同行大見出し］/', '<h2>', $text);
        $text = preg_replace('/［＃同行大見出し終わり］/', '</h2>', $text);
        $text = preg_replace('/［＃同行中見出し］/', '<h3>', $text);
        $text = preg_replace('/［＃同行中見出し終わり］/', '</h3>', $text);
        $text = preg_replace('/［＃同行小見出し］/', '<h4>', $text);
        $text = preg_replace('/［＃同行小見出し終わり］/', '</h4>', $text);
        // 窓見出し
        if (preg_match('/［＃「(.+)」は窓(大|中|小)見出し］/', $text, $str)) {
            if ($str[2] === '大') {
                $text = preg_replace('/' . $str[1] . '/', '<h2>' . $str[1] . '</h2>', $text);
            } else if ($str[2] === '中') {
                $text = preg_replace('/' . $str[1] . '/', '<h3>' . $str[1] . '</h3>', $text);
            } else if ($str[2] === '小') {
                $text = preg_replace('/' . $str[1] . '/', '<h4>' . $str[1] . '</h4>', $text);
            }
            $text = preg_replace('/［＃「(.+)」は窓(大|中|小)見出し］/', '', $text);
        }
        $text = preg_replace('/［＃窓大見出し］/', '<h2>', $text);
        $text = preg_replace('/［＃窓大見出し終わり］/', '</h2>', $text);
        $text = preg_replace('/［＃窓中見出し］/', '<h3>', $text);
        $text = preg_replace('/［＃窓中見出し終わり］/', '</h3>', $text);
        $text = preg_replace('/［＃窓小見出し］/', '<h4>', $text);
        $text = preg_replace('/［＃窓小見出し終わり］/', '</h4>', $text);

        // 外字
        for ($i = 0; $i < 10 && preg_match('/［＃(「.*?」.*?)、第(3|4)水準(.*?)］/u', $text, $str); $i++) {
            $text = preg_replace('/［＃(「.*?」.*?)、第(3|4)水準(.*?)］/u', '(' . $str[1] . ')', $text);
        }

        // 訓点

        // 強調
        // TODO 傍点
        for ($i = 0; $i < 10 && preg_match('/［＃「([^「」]*?)」に傍点］/u', $text, $str); $i++) {
            $text = preg_replace('/' . $str[1] . '［＃「([^「」]*?)」に傍点］/u', '<span class="dot">' . $str[1] . '</span>', $text);
        }
        $text = preg_replace('/［＃傍点］/', '<span class="dot">', $text);
        $text = preg_replace('/［＃傍点終わり］/', '</span>', $text);
        // TODO 傍線
        for ($i = 0; $i < 10 && preg_match('/［＃「([^「」]*?)」に傍線］/u', $text, $str); $i++) {
            $text = preg_replace('/' . $str[1] . '［＃「([^「」]*?)」に傍線］/u', '<u>' . $str[1] . '</u>', $text);
        }
        $text = preg_replace('/［＃傍線］/', '<u>', $text);
        $text = preg_replace('/［＃傍線終わり］/', '</u>', $text);
        // 太字
        for ($i = 0; $i < 10 && preg_match('/［＃「([^「」]*?)」は太字］/u', $text, $str); $i++) {
            $text = preg_replace('/' . $str[1] . '［＃「([^「」]*?)」は太字］/u', '<strong>' . $str[1] . '</strong>', $text);
        }
        $text = preg_replace('/［＃(ここから)?太字］/', '<strong>', $text);
        $text = preg_replace('/［＃(ここで)?太字終わり］/', '</strong>', $text);
        // 斜体
        for ($i = 0; $i < 10 && preg_match('/［＃「([^「」]*?)」は斜体］/u', $text, $str); $i++) {
            $text = preg_replace('/' . $str[1] . '［＃「([^「」]*?)」は斜体］/u', '<em>' . $str[1] . '</em>', $text);
        }
        $text = preg_replace('/［＃(ここから)?斜体］/', '<em>', $text);
        $text = preg_replace('/［＃(ここで)?斜体終わり］/', '</em>', $text);

        // 画面とキャプション
        // 写真や図版、挿絵などの画像
        if (preg_match('/［＃(.*の)(図|地図|絵|挿絵|表|写真).*（(.*?)(、横.*×縦.*)?）入る］/u', $text, $str)) {
            $url = 'https://www.aozora.gr.jp/cards/' . sprintf('%06d', $this->book->person_id) . '/files/' . $str[3];
            $text = preg_replace('/［＃(.*の)(図|地図|絵|挿絵|表|写真).*（(.*)(、横.*×縦.*)?）入る］/u', '<img src="' . $url . '" alt="' . $str[1] . $str[2] . '"></img>', $text);
        }
        // TODO キャプション

        // その他
        // TODO 訂正と「ママ」
        // TODO ルビとルビのように付く文字
        while (preg_match('/｜(.*?)《(.*?)》/u', $text, $str)) {
            $text = preg_replace('/｜(.*?)《(.*?)》/u', '<ruby>' . $str[1] . '<rt>' . $str[2] . '</rt></ruby>', $text);
        }
        while (preg_match('/([\x{3005}\x{3007}\x{303b}\x{3400}-\x{9FFF}\x{F900}-\x{FAFF}\x{20000}-\x{2FFFF}]*)《(.*?)》/u', $text, $str)) {
            $text = preg_replace('/([\x{3005}\x{3007}\x{303b}\x{3400}-\x{9FFF}\x{F900}-\x{FAFF}\x{20000}-\x{2FFFF}]*)《(.*?)》/u', '<ruby>' . $str[1] . '<rt>' . $str[2] . '</rt></ruby>', $text);
        }
        // TODO 縦組み中で横に並んだ文字
        // TODO 割り注
        // TODO 行右小書き、行左小書き文字（縦組み）
        // TODO 上付き小文字、下付き小文字（横組み）
        // TODO 字詰め
        // TODO 罫囲み
        // TODO 横組み
        // 文字サイズ
        if (preg_match('/［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', $text, $str)) {
            if ($str[3] === '大きな') {
                if ($str[2] === '１') {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="large">' . $str[1] . '</span>', $text);
                } else if ($str[2] === '２') {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="x-large">' . $str[1] . '</span>', $text);
                } else {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="xx-large">' . $str[1] . '</span>', $text);
                }
            } else {
                if ($str[2] === '１') {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="small">' . $str[1] . '</span>', $text);
                } else if ($str[2] === '２') {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="x-small">' . $str[1] . '</span>', $text);
                } else {
                    $text = preg_replace('/' . $str[1] . '［＃「(.+?)」は(.+?)段階(大きな|小さな)文字］/u', '<span class="xx-small">' . $str[1] . '</span>', $text);
                }
            }
        }
        if (preg_match('/［＃(.+?)段階(大きな|小さな)文字］/u', $text, $str)) {
            if ($str[2] === '大きな') {
                if ($str[1] === '１') {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="large">', $text);
                } else if ($str[1] === '２') {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="x-large">', $text);
                } else {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="xx-large">', $text);
                }
            } else {
                if ($str[1] === '１') {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="small">', $text);
                } else if ($str[1] === '２') {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="x-small">', $text);
                } else {
                    $text = preg_replace('/［＃(.+?)段階(大きな|小さな)文字］/u', '<span class="xx-small">', $text);
                }
            }
        }
        $text = preg_replace('/［＃(大きな|小さな)文字終わり］/u', '</span>', $text);

        return $text;
    }
}
