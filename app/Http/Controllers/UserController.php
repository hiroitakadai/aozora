<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {
            return Auth::user();
        }

        $res = ["message" => "Unauthorized.", "errors" => ["Auth" => ["ログインしていません。"]]];
        return response()->json($res, 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|regex:/^[a-z0-9]+$/i|same:passwordConfirmation|min:6|max:128'
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);

        Auth::login($user, true);

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
            'password' => 'required|string',
            'newPassword' => 'nullable|regex:/^[a-z0-9]+$/i|same:newPasswordConfirmation|min:6|max:128'
        ]);

        if (!Hash::check($request->input('password'), Auth::user()->password)) {
            $res = ["message" => "Unprocessable Entity.", "errors" => ["Password" => ["パスワードが正しくありません。"]]];
            return response()->json($res, 422);
        }

        $user = User::find(Auth::id());
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->input('new_password')) {
            // TODO セッションを張りなおす
        }

        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], true)) {
            return Auth::user();
        }

        $res = ["message" => "Unauthorized.", "errors" => ["Auth" => ["メールアドレスとパスワードの組み合わせが正しくありません。"]]];
        return response()->json($res, 401);
    }

    public function logout()
    {
        Auth::logout();
    }

    public function indexBooks(Request $request)
    {
        return BookResource::collection(Auth::user()->books()->paginate(10));
    }

    public function updateBooks(Request $request)
    {
        $request->validate([
            'type' => 'in:attach,detach',
            'book' => 'exists:books,id'
        ]);

        Log::debug(Auth::user()->books()->where('books.id', $request->book)->count());

        if ($request->type === 'attach' && Auth::user()->books()->where('books.id', $request->book)->count() == 0) {
            return [Auth::user()->books()->attach($request->book)];
        }

        if ($request->type === 'detach') {
            return [Auth::user()->books()->detach($request->book)];
        }

        return [true];
    }
}
