<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subTitle' => $this->sub_title,
            'classification' => $this->classification,
            'person' => $this->person->full_name,
            'publicated' => $this->original_text_publicated,
            'sentence' => $this->sentence,
            'wordCount' => $this->word_count,
            'comments' => CommentResource::collection($this->comments),
            'favorite' => Auth::user() !== null && Auth::user()->books()->where('books.id', $this->id)->count() > 0
        ];
    }
}
