<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fullName' => $this->full_name,
            'kanaName' => $this->last_name_kana_1 . $this->first_name_kana_1,
            'birth' => date('Y年n月j日', strtotime($this->birth_date)) ?? $this->birth_year . '年',
            'death' => date('Y年n月j日', strtotime($this->death_date)) ?? $this->death_year . '年',
        ];
    }
}
