<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * App\Person
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $last_name
 * @property string|null $first_name
 * @property string|null $last_name_kana_1
 * @property string|null $first_name_kana_1
 * @property string|null $last_name_kana_2
 * @property string|null $first_name_kana_2
 * @property string|null $last_name_romaji
 * @property string|null $first_name_romaji
 * @property int|null $birth_year
 * @property string|null $birth_date
 * @property int|null $death_year
 * @property string|null $death_date
 * @property bool|null $person_copyright_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereBirthYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereDeathDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereDeathYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereFirstNameKana1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereFirstNameKana2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereFirstNameRomaji($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereLastNameKana1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereLastNameKana2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereLastNameRomaji($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person wherePersonCopyrightFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Person whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Person extends Model
{
    //
}
