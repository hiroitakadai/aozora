<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('title_kana_1')->nullable();
            $table->string('title_kana_2')->nullable();
            $table->string('sub_title')->nullable();
            $table->string('sub_title_kana')->nullable();
            $table->string('original_title')->nullable();
            $table->text('first_publicate')->nullable();
            $table->string('classification')->nullable();
            $table->enum('letter_type', ['旧字旧仮名', '旧字新仮名', '新字旧仮名', '新字新仮名', 'その他'])->nullable();
            $table->boolean('copyright_flg')->nullable();
            $table->date('published_date')->nullable();
            $table->date('updated_date')->nullable();
            $table->string('bibliography_url')->nullable();
            $table->bigInteger('person_id')->nullable();
            $table->enum('role', ['著者', '編者', '校訂者', '翻訳者', 'その他'])->nullable();
            $table->string('original_text')->nullable();
            $table->string('original_text_publisher')->nullable();
            $table->string('original_text_publicated')->nullable();
            $table->string('input_edition')->nullable();
            $table->string('proof_edition')->nullable();
            $table->string('inputter')->nullable();
            $table->string('proofreader')->nullable();
            $table->string('text_url')->nullable();
            $table->date('text_updated_at')->nullable();
            $table->string('text_character_code')->nullable();
            $table->string('text_standard')->nullable();
            $table->string('text_update_frequency')->nullable();
            $table->string('html_url')->nullable();
            $table->date('html_updated_at')->nullable();
            $table->string('html_character_code')->nullable();
            $table->string('html_standard')->nullable();
            $table->string('html_update_frequency')->nullable();
            $table->integer('word_count')->nullable();
            $table->text('sentence')->nullable();
            $table->integer('traffic')->nullable();
            $table->enum('category', ['flash', 'novel', 'novelette', 'short', 'shortshort', 'その他'])->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
