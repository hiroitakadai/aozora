<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name_kana_1')->nullable();
            $table->string('first_name_kana_1')->nullable();
            $table->string('last_name_kana_2')->nullable();
            $table->string('first_name_kana_2')->nullable();
            $table->string('last_name_romaji')->nullable();
            $table->string('first_name_romaji')->nullable();
            $table->year('birth_year')->nullable();
            $table->date('birth_date')->nullable();
            $table->year('death_year')->nullable();
            $table->date('death_date')->nullable();
            $table->boolean('person_copyright_flg')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
