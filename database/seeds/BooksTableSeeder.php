<?php

use App\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $read_file_path = __DIR__ . './../../storage/books.csv';
        $file = new SplFileObject($read_file_path, 'r');
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::READ_AHEAD);

        foreach ($file as $i => $row) {
            if ($i === 0) continue;

            foreach ($row as &$string) {
                if ($string === "") {
                    $string = null;
                }
            }

            $book = Book::firstOrNew(["id" => $row[0]]);
            $book->title = $row[1];
            $book->title_kana_1 = $row[2];
            $book->title_kana_2 = $row[3];
            $book->sub_title = $row[4];
            $book->sub_title_kana = $row[5];
            $book->original_text = $row[6];
            $book->first_publicate = $row[7];
            $book->classification = $row[8];
            $book->letter_type = $row[9];
            $book->copyright_flg = ($row[10] === 'あり') ? true : false;
            $book->published_date = $row[11];
            $book->updated_date = $row[12];
            $book->bibliography_url = $row[13];
            $book->person_id = $row[14];
            $book->role = $row[27];
            $book->original_text = $row[28];
            $book->original_text_publisher = $row[29];
            $book->original_text_publicated = $row[30];
            $book->input_edition = $row[31];
            $book->proof_edition = $row[32];
            $book->inputter = $row[44];
            $book->proofreader = $row[45];
            $book->text_url = $row[46];
            $book->text_updated_at = $row[47];
            $book->text_character_code = $row[48];
            $book->text_standard = $row[49];
            $book->text_update_frequency = $row[50];
            $book->html_url = $row[51];
            $book->html_updated_at = $row[52];
            $book->html_character_code = $row[53];
            $book->html_standard = $row[54];
            $book->html_update_frequency = $row[55];
            $book->word_count = $row[56];
            $book->sentence = $row[57];
            $book->traffic = $row[58];
            $book->category = $row[59];
            $book->initial_class = substr($book->classification, 4, 1) === 'K' ? substr($book->classification, 5, 2) : substr($book->classification, 4, 2);
            try {
                $book->save();
            } catch (Exception $e) {
                echo $row[0] . "\n";
            }
        }
    }
}
