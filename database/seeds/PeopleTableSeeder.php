<?php

use App\Person;
use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $read_file_path = __DIR__ . './../../storage/persons.csv';
        $file = new SplFileObject($read_file_path, 'r');
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::READ_AHEAD);

        foreach ($file as $i => $row) {
            if ($i === 0) continue;

            if (config('app.env') === 'production' && $i > 1000) break;

            foreach ($row as &$string) {
                if ($string === "") {
                    $string = null;
                }
            }

            $person = Person::firstOrNew(["id" => $row[0]]);
            $person->full_name = $row[1];
            $person->last_name = $row[2];
            $person->first_name = $row[3];
            $person->last_name_kana_1 = $row[4];
            $person->first_name_kana_1 = $row[5];
            $person->last_name_kana_2 = $row[6];
            $person->first_name_kana_2 = $row[7];
            $person->last_name_romaji = $row[8];
            $person->first_name_romaji = $row[9];
            [$person->birth_year, $person->birth_date] = $this->covert_date($row[10]);
            $person->person_copyright_flg = ($row[12] === 'あり') ? true : false;
            [$person->death_year, $person->death_date] = $this->covert_date($row[11]);

            $person->save();
        }
    }

    private function covert_date($input)
    {
        $year = null;
        $date = null;
        if (mb_strlen($input) === 3 && preg_match("/[1-9][0-9]{2}/", $input)) {
            $year = $input;
        } else if (mb_strlen($input) === 4 && preg_match("/[1-9][0-9]{3}/", $input)) {
            $year = $input;
        } else if (mb_strlen($input) === 9) {
            $year = mb_substr($input, 0, 3);
            $date = $input;
        } else if (mb_strlen($input) === 10) {
            $year = mb_substr($input, 0, 4);
            $date = $input;
        }
        return [$year, $date];
    }
}
