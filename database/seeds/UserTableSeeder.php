<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            ['name' => 'test1', 'email' => 'test1@test.test', 'password' => Hash::make('password')],
            ['name' => 'test2', 'email' => 'test2@test.test', 'password' => Hash::make('password')],
            ['name' => 'test3', 'email' => 'test3@test.test', 'password' => Hash::make('password')],
        );
    }
}
