import { createLocalVue, mount } from '@vue/test-utils'
import Index from '../pages/index.vue'
import BootstrapVue from 'bootstrap-vue'

const localVue = createLocalVue()

localVue.use(BootstrapVue)

describe('index.vue', () => {
  test('test1', () => {
    const wrapper = mount(Index, { localVue })
    expect(wrapper.text()).toContain('Aozora Bunko')
  })
})
