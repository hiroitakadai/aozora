import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import router from './router'
import App from './App.vue'
import store from './store'
import axios from 'axios'

import DefaultLayout from './layouts/DefaultLayout.vue'
import TopLayout from './layouts/TopLayout.vue'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.component('default-layout', DefaultLayout)
Vue.component('top-layout', TopLayout)

const createApp = async (): Promise<void> => {
  await axios.get('/api/users')
    .then(res => {
      store.dispatch('setName', res.data.name)
    }).catch(err => {
      console.log(err)
    })

  new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App />',
  })
}

createApp()
