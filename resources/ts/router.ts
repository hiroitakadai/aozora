import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

import Index from './pages/index.vue'
import Search from './pages/search.vue'
import Author from './pages/author.vue'
import Category from './pages/category.vue'
import Book from './pages/book.vue'
import Login from './pages/login.vue'
import Signup from './pages/signup.vue'
import List from './pages/list.vue'
import Setting from './pages/setting.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    meta: { layout: 'top' },
    component: Index
  },
  {
    path: '/search',
    component: Search
  },
  {
    path: '/author',
    component: Author
  },
  {
    path: '/category',
    component: Category
  },
  {
    path: '/book/:id',
    component: Book
  },
  {
    path: '/login',
    component: Login,
    meta: { logout: true }
  },
  {
    path: '/signup',
    component: Signup,
    meta: { logout: true }
  },
  {
    path: '/list',
    component: List,
    meta: { requiresAuth: true }
  },
  {
    path: '/setting',
    component: Setting,
    meta: { requiresAuth: true }
  }
]

// VueRouterインスタンスを作成する
const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && store.state.name === '') {
    next({ path: '/' })
  } else if (to.matched.some(record => record.meta.logout) && store.state.name != '') {
    next({ path: '/' })
  } else {
    next()
  }
})

export default router
