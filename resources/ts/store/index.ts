import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '',
    redirect: ''
  },

  mutations: {
    setName(state, name): void {
      state.name = name
    },

    setRedirect(state, url): void {
      state.redirect = url
    }
  },

  actions: {
    setName(context, name): void {
      context.commit('setName', name)
    },

    setRedirect(context, url): void {
      context.commit('setRedirect', url)
    }
  }
})
