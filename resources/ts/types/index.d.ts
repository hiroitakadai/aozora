import Vue from 'vue'

declare global {
  interface Book {
    id?: number;
    title?: string | null;
    subTitle?: string | null;
    person?: string | null;
    publicated?: string | null;
    sentence?: string | null;
    wordCount?: number;
    classification?: string | null;
    body?: string | null;
    tail?: string | null;
    comments?: Array<any> | null;
    favorite?: boolean | null;
  }

  interface Author {
    id?: number;
    fullName?: string;
    kanaName?: string;
    birth?: string;
    death?: string;
  }
}
