export const setTitle = (title: string): void => {
  if (title) {
    document.title = title + ' | Aozora Bunko'
  } else {
    document.title = 'Aozora Bunko'
  }
}
