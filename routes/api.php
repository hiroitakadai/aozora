<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('users', 'UserController@store');

Route::middleware('auth:web')->group(function () {
    Route::get('users/books', 'UserController@indexBooks');
    Route::patch('users/books', 'UserController@updateBooks');
    Route::apiResource('users', 'UserController')->only(['index', 'show', 'update']);

    Route::apiresource('comments', 'CommentController')->only(['store', 'update', 'destroy']);

    Route::post('logout', 'UserController@logout');
});


Route::apiresource('books', 'BookController')->only(['index', 'show']);

Route::apiresource('people', 'PersonController')->only(['index', 'show']);
